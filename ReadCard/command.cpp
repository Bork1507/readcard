#include "command.h"

Command::Command()
{

}

void Command::clear()
{
    this->cla = 0;
    this->ins = 0;
    this->p1 = 0;
    this->p2 = 0;
    this->lc = 0;
    this->le = 0;

    this->data.clear();
}

QByteArray Command::getArray()
{
    QByteArray curCommand;
    curCommand.append(this->cla);
    curCommand.append(this->ins);
    curCommand.append(this->p1);
    curCommand.append(this->p2);
    switch (this->ins)
    {
        case 0xA4:
        case 0xA8:
            curCommand.append(this->lc);
            curCommand.append(this->data);
            break;
    }
    curCommand.append(this->le);

    return curCommand;
}

int Command::addData(QByteArray data)
{
    this->data.append(data);
    return this->data.size();
}

int Command::addData(uint8_t data)
{
    this->data.append(data);
    return this->data.size();
}
