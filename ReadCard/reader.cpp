#include "reader.h"

#include <QDebug>
#include <QDate>

//#include <pcsclite.h>

#define BUFFER_SIZE 100

#define CARD_OK 0
#define CARD_ERROR 1

Reader::Reader()
{

}

QStringList Reader::getReades()
{
    LONG result;
    SCARDCONTEXT sc_context;
    DWORD readers_size;
    WCHAR readers[1024];

    QStringList readersList;

    result = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &sc_context);
    if (result != SCARD_S_SUCCESS)
    {
        qDebug("Failed connection to PC/SC established\n");
        qDebug("ERROR = %ld\n", result);
        return readersList;
    }

    result = SCardListReaders(sc_context, NULL, 0, &readers_size);
    if (result != SCARD_S_SUCCESS)
    {
        SCardReleaseContext(sc_context);
        qDebug("ERROR = %ld\n", result);
        return readersList;
    }

    result = SCardListReaders(sc_context, NULL, readers, &readers_size);
    if (result != SCARD_S_SUCCESS) {
        SCardReleaseContext(sc_context);
        qDebug("ERROR = %ld\n", result);
        return readersList;
    }

    int n = 0;
    for (DWORD i = 0; i < readers_size - 1; ++i)
    {
        ++n;
        QString str = QString().fromWCharArray(&readers[i]);
        i += str.length();
        qDebug("  Reader #%d: %s\n", n, str.toStdString().data());
        readersList.append(str);
    }
    qDebug("total: %i\n", n);



    result = SCardReleaseContext(sc_context);
    if (result != SCARD_S_SUCCESS) {
        qDebug("Failed closing connection to PC/SC\n");
        qDebug("ERROR = %ld\n", result);
    }
    else
        qDebug("Connection to PC/SC closed\n");

    return readersList;
}

QByteArray Reader::getPdolData(CardData &cardData)
{
    QByteArray pdolData;
    for(int i=0; i<cardData.pdol.size();)
    {
        switch ((unsigned char)cardData.pdol.at(i++))
        {
            case 0x5f:
                {
                    switch ((unsigned char)cardData.pdol.at(i++))
                    {
                        case 0x2A:
                            {
                                int length = (unsigned char)cardData.pdol.at(i++);
                                if (length == 2)
                                {
                                    uint16_t cur = cardData.getTransactionData().currency.toUShort(NULL, 16);//51;
                                    pdolData.append((cur&0xFF00) >> 8);
                                    pdolData.append(cur&0x00FF);
                                }
                                else
                                    pdolData.append(QByteArray(length, '\0'));
                            }
                            break;
                        default:
                            {
                                int length = (unsigned char)cardData.pdol.at(i++);
                                pdolData.append(QByteArray(length, '\0'));
                            }
                            break;

                    }

                }
                break;
            case 0x9A:
                {
                    int length = (unsigned char)cardData.pdol.at(i++);
                    if (length == 3)
                    {
                        QString date = QDate().currentDate().toString("yyMMdd");
                        pdolData.append(QByteArray::fromHex(date.toUtf8()));
                    }
                    else
                        pdolData.append(QByteArray(length, '\0'));
                }
                break;
            case 0x9f:
                {
                    switch ((unsigned char)cardData.pdol.at(i++))
                    {
                        case 0x02:
                            {
                                int length = (unsigned char)cardData.pdol.at(i++);
                                if (length == 6)
                                {
                                    QString sum = cardData.getTransactionData().sum.rightJustified(12, '0');
                                    pdolData.append(QByteArray::fromHex(sum.toUtf8()));
                                }
                                else
                                    pdolData.append(QByteArray(length, '\0'));
                            }
                            break;
                        case 0x66:
                            {
                                int length = (unsigned char)cardData.pdol.at(i++);
                                if (length == 4)
                                {
                                    pdolData.append(0xf3);
                                    pdolData.append(0x20);
                                    pdolData.append(0x40);
                                    pdolData.append('\0');
                                }
                                else
                                    pdolData.append(QByteArray(length, '\0'));
                            }
                            break;
                        case 0x37:
                            {
                                int length = (unsigned char)cardData.pdol.at(i++);
                                if (length == 4)
                                {
                                    pdolData.append(01);
                                    pdolData.append(01);
                                    pdolData.append(01);
                                    pdolData.append(01);
                                }
                                else
                                    pdolData.append(QByteArray(length, '\0'));
                            }
                            break;
                        default:
                            {
                                int length = (unsigned char)cardData.pdol.at(i++);
                                pdolData.append(QByteArray(length, '\0'));
                            }
                            break;

                    }

                }
                break;
            default:
                {
                    int length = (unsigned char)cardData.pdol.at(i++);
                    pdolData.append(QByteArray(length, '\0'));
                }
                break;
        }
    }
    return pdolData;
}

int Reader::getLength(QByteArray data, int *length)
{
    int shift = 0;
    *length = 0;
    if (data.length() == 0)
        return shift;

    switch ((unsigned char)data.at(0))
    {
        case 0x81:
            if ((unsigned char)data.length() < 3)
                return shift;

            shift = 2;
            *length = (unsigned char)data.at(1);
            break;
        case 0x82:
            if ((unsigned char)data.length() < 4)
                return shift;

            shift = 3;
            *length = (unsigned char)data.at(1) << 8;
            *length += (unsigned char)data.at(2);
            break;
        case 0x83:
            if ((unsigned char)data.length() < 5)
                return shift;

            shift = 4;
            *length = (unsigned char)data.at(1) << 16;
            *length += (unsigned char)data.at(2) << 8;
            *length += (unsigned char)data.at(3);
            break;
        case 0x84:
            if ((unsigned char)data.length() < 6)
                return shift;

            shift = 5;
            *length = (unsigned char)data.at(1) << 24;
            *length += (unsigned char)data.at(2) << 16;
            *length += (unsigned char)data.at(3) << 8;
            *length += (unsigned char)data.at(4);
            break;

        default:
            if ((unsigned char)data.length() < 2)
                return shift;

            shift = 1;
            *length = (unsigned char)data.at(0);
            break;

    }

    return shift;
}
int Reader::parser(QByteArray resultData, CardData &cardData)
{
    int i=0;
    int messageLength = 0;
    switch ((unsigned char)resultData.at(i++))
    {
        case 0x6F: // File Control Information (FCI) Template
            i += getLength(resultData.mid(i), &messageLength);
            break;
        case 0x70: // READ RECORD Response Message Template
            i += getLength(resultData.mid(i), &messageLength);
            break;
        case 0x77: // Response Message Template Format 2
            i += getLength(resultData.mid(i), &messageLength);
            break;
    }

    for(; i<messageLength;)
    {
        int dummy;
        switch ((unsigned char)resultData.at(i))
        {
            // from template 0x70
            case 0x61: // Application Template
                {
                    i++;
                    i += getLength(resultData.mid(i), &dummy);
                }
                break;
            // from template 0x6F
            case 0xA5: // File Control Information (FCI) Proprietary Template
                {
                    i++;
                    i += getLength(resultData.mid(i), &dummy);
                }
                break;







            case 0x4F: // Application Dedicated File (ADF) Name
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.adf.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x50: // Application Label
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);

                    cardData.appLabel = QString(resultData.mid(i, length));
                    i += length;
                }
                break;
            case 0x57: // Track 2 Equivalent Data
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.track2.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x5A: // Application Primary Account Number (PAN)
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.pan = QString(resultData.mid(i, length).toHex());
                    i += length;
                }
                break;
            case 0x5F: // first byte of tag
                {
                    i++;
                    switch ((unsigned char)resultData.at(i)) // second byte of tag
                    {
                        case 0x20: // Cardholder Name
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.embossedName = QString(resultData.mid(i, length));
                                i += length;
                            }
                            break;
                        case 0x24: // Application Expiration Date
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.appExpDate.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x25: // Application Effective Date
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.appEffectDate.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x28: // Issuer Country Code
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.issCountryCode.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x2D: // Language Preference
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.lang = QString(resultData.mid(i, length));
                                i += length;
                            }
                            break;
                        case 0x30: // Service Code
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.serviceCode.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x34: // Application Primary Account Number (PAN) Sequence Number
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.panSeqNum.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        default: // Unknown tags
                            {
                                cardData.unknownTags += "5F";
                                cardData.unknownTags += QString(resultData.mid(i, 1).toHex());
                                cardData.unknownTags += ".";

                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                i += length;
                            }
                            break;
                    }
                }
                break;
            case 0x82: // Application Interchange Profile
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.appIntProf.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x84: // Short File Identifier (SFI)
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.df.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x87: // Application Priority Indicator
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.appPriorInd.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x88: // Dedicated File (DF) Name
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.sfi.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x8C: // Card Risk Management Data Object List 1 (CDOL1)
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.cdol1.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x8D: // Card Risk Management Data Object List 2 (CDOL2)
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.cdol2.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x8E: // Cardholder Verification Method (CVM) List
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.cvm.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x8F: // Certification Authority Public Key Index
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.certAuthPubKeyIndex.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x90: // Issuer Public Key Certificate
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.issPubKeyCert.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x92: // Issuer Public Key Remainder
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    cardData.issPubKeyRem.append(resultData.mid(i, length).data(), length);
                    i += length;
                }
                break;
            case 0x94: // Application File Locator (AFL)
                {
                    i++;
                    int length;
                    i += getLength(resultData.mid(i), &length);
                    for(int a=0; a<length; a+=4)
                    {
                        QByteArray afl;
                        afl.append(resultData.mid(i, 4).data(), 4);
                        i += 4;
                        cardData.afls.append(afl);
                    }
                }
                break;
            case 0x9F: // first byte of tag
                {
                    i++;
                    switch ((unsigned char)resultData.at(i)) // second byte of tag
                    {

                        case 0x07: // Application Usage Control
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.appUseControl.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x08: // Application Version Number
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.appVerNum.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x0D: // Issuer Action Code - Default
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.issActCodeDef.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x0E: // Issuer Action Code - Denial
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.issActCodeDen.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x0F: // Issuer Action Code - Online
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.issActCodeOnl.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x10: // Issuer Application Data
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.issAppData.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x1F: // Track 1 Discretionary Data
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.track1DiscData.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x26: // Application Cryptogram
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.appCrypt.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x27: // Cryptogram Information Data
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.cryptInfData.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x32: // Issuer Public Key Exponent
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.issPubKeyExp.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x36: // Application Transaction Counter (ATC)
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.appTransCount.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x38: // Processing Options Data Object List (PDOL)
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.pdol.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x44: // Application Currency Exponent
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.appCurrExpon.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x46: // ICC Public Key Certificate
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.pubKeyCert.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x47: // ICC Public Key Exponent
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.pubKeyExp.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x48: // ICC Public Key Remainder
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.pubKeyRem.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x49: // Dynamic Data Authentication Data Object List (DDOL)
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.authDataObjectList.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x4A: // Static Data Authentication Tag List
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.staticDataAuthTagList.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        case 0x4B: // Signed Dynamic Application Data
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.signDynAppData.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        default: // Unknown tags
                            {
                                cardData.unknownTags += "9F";
                                cardData.unknownTags += QString(resultData.mid(i, 1).toHex());
                                cardData.unknownTags += ".";

                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                i += length;
                            }
                            break;
                    }
                }
                break;
            case 0xBF: // first byte of tag
                {
                    i++;
                    switch ((unsigned char)resultData.at(i)) // second byte of tag
                    {
                        case 0x0C: // File Control Information (FCI) Issuer Discretionary Data
                            {
                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                cardData.issDiscretData.append(resultData.mid(i, length).data(), length);
                                i += length;
                            }
                            break;
                        default: // Unknown tags
                            {
                                cardData.unknownTags += "BF";
                                cardData.unknownTags += QString(resultData.mid(i, 1).toHex());
                                cardData.unknownTags += ".";

                                i++;
                                int length;
                                i += getLength(resultData.mid(i), &length);
                                i += length;
                            }
                            break;
                    }
                }
                break;
            default: // Unknown tags
                {
                    cardData.unknownTags += QString(resultData.mid(i, 1).toHex());
                    cardData.unknownTags += ".";

                    int length = (unsigned char)resultData.at(++i);
                    i++;
                    i += length;
                }
                break;
        }
    }
}
int Reader::sendCommand(Command &command, CardData &cardData)
{
    LONG result;

    qDebug("\n\nsendCommand started\n");

    BYTE recv_buffer[BUFFER_SIZE*3] = {0};
    DWORD base_length = 0, recv_length = 0;
    switch (command.ins)
    {
        case 0xB2:
            base_length = recv_length = BUFFER_SIZE*3;
            break;
        default:
            base_length = recv_length = BUFFER_SIZE*3;
        break;
    }

    qDebug("send - %s\n", command.getArray().toHex('|').data());
    result = SCardTransmit(_reader, _scard_pci,
        (BYTE*)command.getArray().data(), command.getArray().size(), NULL,
        recv_buffer, &recv_length);
    if (result != SCARD_S_SUCCESS) {
        qDebug("sendCommand ERROR = %x\n", result);
        return CARD_ERROR;
    }

    for(DWORD i=0;i<recv_length;i++)
        qDebug("recv_buffer[%d] = %02x = %c\n", i, recv_buffer[i], recv_buffer[i]);

    if ( !( recv_buffer[recv_length-2] == 0x90 && recv_buffer[recv_length-1] == 0) )
    {

        if (recv_buffer[recv_length-2] == 0x61 )
        {
            command.clear();
            command.cla = 0;
            command.ins = 0xC0;
            command.p1 = 0;
            command.p2 = 0;
            command.le = recv_buffer[recv_length-1];
        }
        else if (recv_buffer[recv_length-2] == 0x6C )
        {
            command.le = recv_buffer[recv_length-1];
        }
        else
            return CARD_ERROR;

        qDebug("sendCommand continue\n");

        recv_length = base_length;

        memset(recv_buffer, 0, sizeof(recv_buffer));

        qDebug("send - %s\n", command.getArray().toHex('|').data());
        result = SCardTransmit(_reader, SCARD_PCI_T0,
            (BYTE*)command.getArray().data(), command.getArray().size(), NULL,
            recv_buffer, &recv_length);
        if (result != SCARD_S_SUCCESS) {
            qDebug("sendCommand ERROR = %x\n", result);
            return CARD_ERROR;
        }

        for(DWORD i=0;i<recv_length;i++)
            qDebug("recv_buffer[%d] = %02x = %c\n", i, recv_buffer[i], recv_buffer[i]);
    }


    parser(QByteArray((const char*)recv_buffer, recv_length), cardData);

    qDebug("sendCommand stopped\n");
    return CARD_OK;
}

void Reader::readCard(QString reader, CardData &cardData)
{
    LONG result;
    WCHAR reader_name[256] = {0};

    int error = CARD_OK;

    if (reader.length() > 1) {
        wcsncpy(reader_name, reader.toStdWString().data(), reader.length());
    } else {
        qDebug("No readers found!\n");
        return;
    }

    result = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &_sc_context);
    if (result != SCARD_S_SUCCESS)
    {
        qDebug("Failed connection to PC/SC established\n");
        qDebug("ERROR = %ld\n", result);
    }


    SCARD_READERSTATE sc_reader_states[1];
    sc_reader_states[0].szReader = reader_name;
    sc_reader_states[0].dwCurrentState = SCARD_STATE_EMPTY;

    result = SCardGetStatusChange(_sc_context, INFINITE, sc_reader_states, 1);
    if (result != SCARD_S_SUCCESS) {
        SCardReleaseContext(_sc_context);
        qDebug("ERROR = %ld\n", result);
    }


    DWORD active_protocol;

    result = SCardConnect(_sc_context, reader_name,
        SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
        &_reader, &active_protocol);
    if (result != SCARD_S_SUCCESS) {
        SCardReleaseContext(_sc_context);
        qDebug("ERROR = %ld\n", result);
    }

    if (active_protocol == SCARD_PROTOCOL_T0) {
        qDebug("  Card connected, selected protocol: T=0.\n");
        _scard_pci = SCARD_PCI_T0;
    }
    if (active_protocol == SCARD_PROTOCOL_T1) {
        qDebug("  Card connected, selected protocol: T=1.\n");
        _scard_pci = SCARD_PCI_T1;
    }



    // STATUS
    WCHAR reader_friendly_name[255/*MAX_READERNAME*/];
    DWORD reader_friendly_name_size = 255;//MAX_READERNAME;
    DWORD state;
    DWORD protocol;
    DWORD atr_size = 255;//MAX_ATR_SIZE;
    BYTE atr[255/*MAX_ATR_SIZE*/];

    result = SCardStatus(_reader, reader_friendly_name, &reader_friendly_name_size,
        &state, &protocol, atr, &atr_size);
    if (result != SCARD_S_SUCCESS)
    {
        SCardDisconnect(_reader, SCARD_RESET_CARD);
        SCardReleaseContext(_sc_context);
        qDebug("ERROR = %ld\n", result);
        return;
    }

    qDebug("  Card ATR: \n");
    for(int i=0;i<atr_size;i++)
        qDebug("atr = %02x = %c\n", atr[i], atr[i]);


    Command command;

    // STATUS
    if(error == CARD_OK)
    {
        command.clear();
        command.cla = 0x00;
        command.ins = 0xCA;
        command.p1 = 0x00;
        command.p2 = 0x00;
        command.le = 0x00;
        error = sendCommand(command, cardData);
        error = CARD_OK;
    }

    // ГЛАВНЫЙ КАТАЛОГ
    if(error == CARD_OK)
    {
        command.clear();
        command.cla = 0x00;
        command.ins = 0xA4;
        command.p1 = 0x04;
        command.p2 = 0x00;
        command.lc = command.addData("1PAY.SYS.DDF01");
        command.le = 0x00;
        error = sendCommand(command, cardData);
    }

    // СПИСОК ПРИЛОЖЕНИЙ
    if(error == CARD_OK)
    {
        for(int j=0; j<10;j++)
        {
            command.clear();
            command.cla = 0x00;
            command.ins = 0xB2;
            command.p1 = j+1;
            command.p2 = 0x0C;
            command.le = 0x00;
            error = sendCommand(command, cardData);
            if(error == CARD_ERROR)
            {
                if(j>0)
                    error = CARD_OK;
                break;
            }
        }
    }

    // ПОЛУЧАЕМ PDOL
    if(error == CARD_OK)
    {
        command.clear();
        command.cla = 0x00;
        command.ins = 0xA4;
        command.p1 = 0x04;
        command.p2 = 0x00;
        command.lc = command.addData(cardData.adf);
        command.le = 0x00;
        error = sendCommand(command, cardData);
    }

    // ПОЛУЧАЕМ ПАРАМЕТРЫ ТРАНЗАКЦИИ
    //    BYTE send_buffer5[] = {0x80, 0xA8, 0x00, 0x00, 0x15, 0x83, 0x13,
    //                           0xf3, 0x20, 0x40, 0x00,
    //                           0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
    //                           0x01, 0x01, 0x01, 0x01,
    //                           0x00, 51,
    //                           19, 03, 01,
    //                           0x00};
    //    BYTE send_buffer5[] = {0x80, 0xA8, 0x00, 0x00, 0x02, 0x83, 0x00, 0x00};
    if(error == CARD_OK)
    {
        QByteArray pdolData = getPdolData(cardData);
        command.clear();
        command.cla = 0x80;
        command.ins = 0xA8;
        command.p1 = 0x00;
        command.p2 = 0x00;
        command.lc = command.addData(0x83);
        command.lc = command.addData(pdolData.size());
        command.lc = command.addData(pdolData);
        command.le = 0x00;
        error = sendCommand(command, cardData);
    }



//   если
//   recv_buffer5 = 94 = ? = AFL
//   recv_buffer5 = 04 =  = AFL length
//
//   recv_buffer5 = 10 =  = EFI
//   recv_buffer5 = 02 =  = start
//   recv_buffer5 = 03 =  = stop
//   recv_buffer5 = 00 =

    if(error == CARD_OK)
    {
        for(int i = 0; i< cardData.afls.size(); i++)
        {
            qDebug("\n\n!!!!!!!!!!!!!!!!!!! SFI = 0x%02x P2 = 0x%02x\n", cardData.afls.at(i).at(0), (cardData.afls.at(i).at(0) | 4));
            for(int j = cardData.afls.at(i).at(1); j< cardData.afls.at(i).at(2)+1; j++)
            {
                command.clear();
                command.cla = 0x00;
                command.ins = 0xB2;
                command.p1 = j;
                command.p2 = (cardData.afls.at(i).at(0) | 4);
                command.le = 0x00;
                error = sendCommand(command, cardData);
                if(error == CARD_ERROR)
                    break;
            }
        }
    }


    SCardDisconnect(_reader, SCARD_RESET_CARD);
    SCardReleaseContext(_sc_context);
    qDebug("Connection to PC/SC closed\n");
}
