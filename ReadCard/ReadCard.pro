#-------------------------------------------------
#
# Project created by QtCreator 2020-02-07T23:09:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ReadCard
TEMPLATE = app


SOURCES += main.cpp\
        carddata.cpp \
        command.cpp \
        mainwindow.cpp \
        reader.cpp \
        transactiondata.cpp

HEADERS  += mainwindow.h \
    carddata.h \
    command.h \
    reader.h \
    transactiondata.h

FORMS    += mainwindow.ui

LIBS += -L"$$PWD/lib/" -lWinscard
