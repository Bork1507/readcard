#ifndef COMMAND_H
#define COMMAND_H

#include <winscard.h>
#include <QByteArray>

class Command
{
public:
    BYTE cla;
    BYTE ins;
    BYTE p1;
    BYTE p2;
    BYTE lc;
    BYTE le;

    Command();

    void clear();
    QByteArray getArray();
    int addData(QByteArray data);
    int addData(uint8_t data);

private:
    QByteArray data;

};

#endif // COMMAND_H
