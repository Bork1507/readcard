#ifndef CARDDATA_H
#define CARDDATA_H

#include <QByteArray>
#include <QList>

#include "transactiondata.h"

#define ADF_MAX_SIZE 16

class CardData
{
public:
    CardData();

    QByteArray adf;                   // tag 4F   - Application Dedicated File (ADF) Name
    QString appLabel;                 // tag 50   - Application Label
    QByteArray track2;                // tag 57   - Track 2 Equivalent Data
    QString pan;                      // tag 5A   - Application Primary Account Number (PAN)
    QString embossedName;             // tag 5F20 - Cardholder Name
    QByteArray appExpDate;            // tag 5F24 - Application Expiration Date
    QByteArray appEffectDate;         // tag 5F25 - Application Effective Date
    QByteArray issCountryCode;        // tag 5F28 - Issuer Country Code
    QString lang;                     // tag 5F2D - Language Preference
    QByteArray serviceCode;           // tag 5F30 - Service Code
    QByteArray panSeqNum;             // tag 5F34 - Application Primary Account Number (PAN) Sequence Number
    QByteArray appIntProf;            // tag 82   - Application Interchange Profile
    QByteArray df;                    // tag 84   - Dedicated File (DF) Name
    QByteArray appPriorInd;           // tag 87   - Application Priority Indicator
    QByteArray sfi;                   // tag 88   - Short File Identifier (SFI)
    QByteArray cdol1;                 // tag 8C   - Card Risk Management Data Object List 1 (CDOL1)
    QByteArray cdol2;                 // tag 8D   - Card Risk Management Data Object List 2 (CDOL2)
    QByteArray cvm;                   // tag 8E   - Cardholder Verification Method (CVM) List
    QByteArray certAuthPubKeyIndex;   // tag 8F   - Certification Authority Public Key Index
    QByteArray issPubKeyCert;         // tag 90   - Issuer Public Key Certificate
    QByteArray issPubKeyRem;          // tag 92   - Issuer Public Key Remainder
    QList<QByteArray> afls;           // tag 94   - Application File Locator (AFL)
    QByteArray appUseControl;         // tag 9F07 - Application Usage Control
    QByteArray appVerNum;             // tag 9F08 - Application Version Number
    QByteArray issActCodeDef;         // tag 9F0D - Issuer Action Code - Default
    QByteArray issActCodeDen;         // tag 9F0E - Issuer Action Code - Denial
    QByteArray issActCodeOnl;         // tag 9F0F - Issuer Action Code - Online
    QByteArray issAppData;            // tag 9F10 - Issuer Application Data
    QByteArray track1DiscData;        // tag 9F1F - Track 1 Discretionary Data
    QByteArray appCrypt;              // tag 9F26 - Application Cryptogram
    QByteArray cryptInfData;          // tag 9F27 - Cryptogram Information Data
    QByteArray issPubKeyExp;          // tag 9F32 - Issuer Public Key Exponent
    QByteArray appTransCount;         // tag 9F36 - Application Transaction Counter (ATC)
    QByteArray pdol;                  // tag 9F38 - Processing Options Data Object List (PDOL)
    QByteArray appCurrExpon;          // tag 9F44 - Application Currency Exponent
    QByteArray pubKeyCert;            // tag 9F46 - ICC Public Key Certificate
    QByteArray pubKeyExp;             // tag 9F47 - ICC Public Key Exponent
    QByteArray pubKeyRem;             // tag 9F48 - ICC Public Key Remainder
    QByteArray authDataObjectList;    // tag 9F49 - Dynamic Data Authentication Data Object List (DDOL)
    QByteArray staticDataAuthTagList; // tag 9F4A - Static Data Authentication Tag List
    QByteArray signDynAppData;        // tag 9F4B - Signed Dynamic Application Data
    QByteArray issDiscretData;        // tag BF0C - File Control Information (FCI) Issuer Discretionary Data




    QString unknownTags;


    void addTransactionData(TransactionData data);
    TransactionData& getTransactionData(){return _transData;};


private:
    TransactionData _transData;
};

#endif // CARDDATA_H
