#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("Read card");

    ui->label_transSum->setVisible(false);
    ui->lineEdit_transSum->setVisible(false);
    ui->label_transCurCode->setVisible(false);
    ui->lineEdit_transCurCode->setVisible(false);

    QStringList readers = reader.getReades();

    if (readers.length()==0)
    {
        ui->comboBox_readers->setEnabled(false);
        ui->pushButton_readCard->setEnabled(false);
    }
    else
    {
        ui->comboBox_readers->addItems(readers);
    }
}

void MainWindow::on_pushButton_readCard_clicked()
{
    TransactionData transData;
    transData.sum = ui->lineEdit_transSum->text();
    transData.currency = ui->lineEdit_transCurCode->text();
    CardData cardData;
    cardData.addTransactionData(transData);
    reader.readCard(ui->comboBox_readers->currentText(), cardData);

    ui->textEdit_result->clear();
    ui->textEdit_result->append("tag 50   Application Label - " + cardData.appLabel);
    ui->textEdit_result->append("tag 57   Track 2 - " + cardData.track2.toHex());
    ui->textEdit_result->append("tag 5A   PAN - " + cardData.pan);
    ui->textEdit_result->append("tag 5F20 Cardholder Name - " + cardData.embossedName);
    ui->textEdit_result->append("tag 5F24 Application Expiration Date - " + cardData.appExpDate.toHex('.'));
    ui->textEdit_result->append("tag 5F25 Application Effective Date - " + cardData.appEffectDate.toHex('.'));
    ui->textEdit_result->append("tag 5F28 Issuer Country Code - " + cardData.issCountryCode.toHex('.'));
    ui->textEdit_result->append("tag 5F2D Language Preference - " + cardData.lang);
    ui->textEdit_result->append("tag 5F30 Service Code - " + cardData.serviceCode.toHex());
    ui->textEdit_result->append("tag 5F34 Application Primary Account Number (PAN) Sequence Number - " + cardData.panSeqNum.toHex('.'));
    ui->textEdit_result->append("tag 82   Application Interchange Profile - " + cardData.appIntProf.toHex('.'));
    ui->textEdit_result->append("tag 87   Application Priority Indicator - " + cardData.appPriorInd.toHex('.'));
    ui->textEdit_result->append("tag 88   Short File Identifier (SFI) - " + cardData.authDataObjectList.toHex('.'));
    ui->textEdit_result->append("tag 8C   Card Risk Management Data Object List 1 (CDOL1) - " + cardData.cdol1.toHex('.'));
    ui->textEdit_result->append("tag 8D   Card Risk Management Data Object List 2 (CDOL2) - " + cardData.cdol2.toHex('.'));
    ui->textEdit_result->append("tag 8E   Cardholder Verification Method (CVM) List - " + cardData.cvm.toHex('.'));
    ui->textEdit_result->append("tag 8F   Certification Authority Public Key Index - " + cardData.certAuthPubKeyIndex.toHex('.'));
    ui->textEdit_result->append("tag 90   Issuer Public Key Certificate - " + cardData.issPubKeyCert.toHex('.'));
    ui->textEdit_result->append("tag 92   Issuer Public Key Remainder - " + cardData.issPubKeyRem.toHex('.'));
    ui->textEdit_result->append("tag 9F07 Application Usage Control - " + cardData.appUseControl.toHex('.'));
    ui->textEdit_result->append("tag 9F08 Application Version Number - " + cardData.appVerNum.toHex('.'));
    ui->textEdit_result->append("tag 9F0D Issuer Action Code - Default - " + cardData.issActCodeDef.toHex('.'));
    ui->textEdit_result->append("tag 9F0E Issuer Action Code - Denial - " + cardData.issActCodeDen.toHex('.'));
    ui->textEdit_result->append("tag 9F0F Issuer Action Code - Online - " + cardData.issActCodeOnl.toHex('.'));
    ui->textEdit_result->append("tag 9F10 Issuer Application Data - " + cardData.issAppData.toHex('.'));
    ui->textEdit_result->append("tag 9F1F Track 1 Discretionary Data - " + cardData.track1DiscData);
    ui->textEdit_result->append("tag 9F26 Application Cryptogram - " + cardData.appCrypt.toHex('.'));
    ui->textEdit_result->append("tag 9F27 Cryptogram Information Data - " + cardData.cryptInfData.toHex('.'));
    ui->textEdit_result->append("tag 9F32 Issuer Public Key Exponent - " + cardData.issPubKeyExp.toHex('.'));
    ui->textEdit_result->append("tag 9F36 Application Transaction Counter (ATC) - " + cardData.appTransCount.toHex('.'));
    ui->textEdit_result->append("tag 9F44 Application Currency Exponent - " + cardData.appCurrExpon.toHex('.'));
    ui->textEdit_result->append("tag 9F46 ICC Public Key Certificate - " + cardData.pubKeyCert.toHex('.'));
    ui->textEdit_result->append("tag 9F47 ICC Public Key Exponent - " + cardData.pubKeyExp.toHex('.'));
    ui->textEdit_result->append("tag 9F48 ICC Public Key Remainder - " + cardData.pubKeyRem.toHex('.'));
    ui->textEdit_result->append("tag 9F49 Dynamic Data Authentication Data Object List (DDOL) - " + cardData.authDataObjectList.toHex('.'));
    ui->textEdit_result->append("tag 9F4A Static Data Authentication Tag List - " + cardData.staticDataAuthTagList.toHex('.'));
    ui->textEdit_result->append("tag 9F4B Signed Dynamic Application Data - " + cardData.signDynAppData.toHex('.'));
    ui->textEdit_result->append("tag BF0C File Control Information (FCI) Issuer Discretionary Data - " + cardData.issDiscretData.toHex('.'));




    ui->textEdit_result->append("Unknown tags - " + cardData.unknownTags);
}

MainWindow::~MainWindow()
{
    delete ui;
}
