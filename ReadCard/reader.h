#ifndef READER_H
#define READER_H

#include <QStringList>

#include <winscard.h>
#include "command.h"
#include "carddata.h"

class Reader
{
public:
    Reader();

    QStringList getReades();
    void readCard(QString reader, CardData &cardData);

private:
    SCARDHANDLE _reader;
    SCARDCONTEXT _sc_context;
    LPCSCARD_IO_REQUEST _scard_pci;

    int getLength(QByteArray data, int *length);
    int sendCommand(Command &command, CardData &cardData);
    int parser(QByteArray resultData, CardData &cardData);
    QByteArray getPdolData(CardData &cardData);
};

#endif // READER_H
